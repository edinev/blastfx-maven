module com.emildinev {
    requires javafx.controls;
    requires javafx.fxml;
    //requires jlfgr10;
    requires gson;
    requires java.sql;

    opens com.emildinev to javafx.fxml;
    exports com.emildinev;
}