package com.emildinev;

public abstract class CoordinatePoint
{
//    Coordinate
    private int number;
    private double north;
    private double east;
    private double elevation;

    public int getNumber()
    {
        return number;
    }

    public void setNumber(int number)
    {
        this.number = number;
    }

    public double getNorth()
    {
        return north;
    }

    public void setNorth(double north)
    {
        this.north = north;
    }

    public double getEast()
    {
        return east;
    }

    public void setEast(double east)
    {
        this.east = east;
    }

    public double getElevation()
    {
        return elevation;
    }

    public void setElevation(double elevation)
    {
        this.elevation = elevation;
    }

    public CoordinatePoint(int number, double north, double east, double elevation)
    {
        this.number = number;
        this.north = north;
        this.east = east;
        this.elevation = elevation;
    }

    @Override
    public String toString()
    {
        return
                "number=" + number +
                ", north=" + north +
                ", east=" + east +
                ", elevation=" + elevation;
    }
}
