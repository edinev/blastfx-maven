package com.emildinev;

import java.util.ArrayList;

public class DrillField<T extends CoordinatePoint>
{
    private String name;
    private double benchElevation;
    private double bottomBench;

    private int holesDiameter;
    private double holesSubDrill;
    private int holesInclination;

    private double patternA;
    private double patternB;
    private ArrayList<T> members = new ArrayList<T>();

    public String getName()
    {
        return name;
    }

    public double getBenchElevation()
    {
        return benchElevation;
    }

    public double getBottomBench()
    {
        return bottomBench;
    }

    public int getHolesDiameter()
    {
        return holesDiameter;
    }

    public double getHolesSubDrill()
    {
        return holesSubDrill;
    }

    public int getHolesInclination()
    {
        return holesInclination;
    }

    public double getPatternA()
    {
        return patternA;
    }

    public double getPatternB()
    {
        return patternB;
    }

    public ArrayList<T> getMembers()
    {
        return members;
    }

    private int pointsCount = 0;

    public int getPointsCount()
    {
        return pointsCount;
    }

    public DrillField(String name)
    {
        this.name = name;
    }

    public DrillField(String name, double benchElevation, double bottomBench, int holesDiameter, double holesSubDrill,
                      int holesInclination, double patternA, double patternB)
    {
        this.name = name;
        this.benchElevation = benchElevation;
        this.bottomBench = bottomBench;
        this.holesDiameter=holesDiameter;
        this.holesSubDrill = holesSubDrill;
        this.holesInclination = holesInclination;
        this.patternA=patternA;
        this.patternB=patternB;
    }

    /**
     * Add T point to the group.
     * Checks if T point has a negative point number
     * Checks if T point already exists in the group.
     * If exists return FALSE
     * @param point
     * @return
     */
    public boolean addPoint(T point)
    {
        if (point.getNumber() <= 0) {
            System.out.println("Negative point");
            return false;
        }

        if (findPoint(point.getNumber()) != null) {
            System.out.println(point.getNumber() + "  already in group " + this.name);
            return false;
        } else {
            members.add(point);
            pointsCount++;
            return true;
        }
    }

    public boolean removePoint(int pointNumber)
    {
        if (findPoint(pointNumber) != null) {
            members.remove(pointNumber - 1);
            System.out.println("\nPoint " + pointNumber + " has been removed");
            pointsCount--;
            return true;
        }
        return false;
    }

    private T findPoint(int pointNumber)
    {
        for (T checkedPoint : this.members) {
            if (checkedPoint.getNumber() == pointNumber) {
                return checkedPoint;
            }
        }
        return null;
    }

}
