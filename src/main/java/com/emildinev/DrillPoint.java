package com.emildinev;

public class DrillPoint extends CoordinatePoint
{
//    Drilling
    private int diameter;
    private double length;
    private double subDrill;
    private int inclination;

    public DrillPoint(int number, double north, double east, double elevation, int diameter, double length, double subDrill, int inclination)
    {
        super(number, north, east, elevation);
        this.diameter = diameter;
        this.length = length;
        this.subDrill = subDrill;
        this.inclination = inclination;
    }

    public DrillPoint(int number, double north, double east, double elevation)
    {
        super(number, north, east, elevation);
    }

    @Override
    public String toString()
    {
        return super.toString() +
                "\nDiameter: " + this.diameter +
                "\nSubdrilling: " + this.subDrill +
                "\nInclination: " + this.inclination;
    }
}

