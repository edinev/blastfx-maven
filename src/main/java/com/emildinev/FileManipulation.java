package com.emildinev;

import com.emildinev.DrillField;
import com.google.gson.Gson;

import java.io.FileWriter;
import java.io.IOException;

public class FileManipulation
{
    public static void exportPointGroupToJson(DrillField fieldName, String fileName) throws IOException
    {
        System.out.println("1. Start");
        Gson gson = new Gson();
        System.out.println("2. Next");
        gson.toJson(fieldName, new FileWriter(fileName));
    }
}
