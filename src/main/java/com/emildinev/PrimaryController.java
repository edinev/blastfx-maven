package com.emildinev;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.ArrayList;

public class PrimaryController {

    @FXML
    private TextField textField;
    @FXML
    private Button helloButton;
    @FXML
    private Button byeButton;
    @FXML
    private CheckBox ourCheckBox;
    @FXML
    private Label ourLabel;
    @FXML
    private Button createBlastFieldButton;
    @FXML
    private ComboBox comboBoxBlastFieldTab;
    @FXML
    private Button refreshButton;
    @FXML
    private TextArea textAreaBlastFieldTab;
    @FXML
    private ListView listViewPoints;






    public void initialize()
    {
        helloButton.setDisable(true);
        byeButton.setDisable(true);

        ArrayList<DrillField> drillFields = new ArrayList<>();
        comboBoxBlastFieldTab.getItems().addAll(drillFields);
    }

    public void clickCreateDrillFieldButton() throws IOException
    {
        Parent part = FXMLLoader.load(getClass().getResource("createBlastFieldWindow.fxml"));
        Stage createBlastFieldStage = new Stage();
        Scene scene = new Scene(part);
        createBlastFieldStage.setScene(scene);
        createBlastFieldStage.setTitle("Create Drill field");
        createBlastFieldStage.getIcons().add(new Image("/toolbarButtonGraphics/general/New24.gif"));
        createBlastFieldStage.setResizable(false);

        createBlastFieldStage.initModality(Modality.APPLICATION_MODAL);
        createBlastFieldStage.show();


    }

    public void refreshTextArea()
    {
    }

    public void onListViewPointsClick()
    {
        String selectedHole = listViewPoints.getSelectionModel().getSelectedItem().toString();
        System.out.println("Selected hole number is: " + selectedHole);
        String selectedField = comboBoxBlastFieldTab.getSelectionModel().getSelectedItem().toString();
        System.out.println("Selected blast field is: " + selectedField);

    }


    @FXML
    public void onButtonClick(ActionEvent e)
    {
        if (e.getSource().equals(helloButton)) {
            System.out.println("Hello, " + textField.getText());
        } else if (e.getSource().equals(byeButton)) {
            System.out.println("Bye, " + textField.getText());
        }

        if (ourCheckBox.isSelected()) {
            textField.clear();
            helloButton.setDisable(true);
            byeButton.setDisable(true);
        }
    }

    @FXML
    public void handleKeyRelease()
    {
        String text = textField.getText();
        boolean disableButtons = text.isEmpty() || text.trim().isEmpty();

        helloButton.setDisable(disableButtons);
        byeButton.setDisable(disableButtons);
    }

    public void handleChange()
    {
        System.out.println("The checkBox is " + (ourCheckBox.isSelected() ? "checked" : "not checked"));
    }

}
