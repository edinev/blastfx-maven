package com.emildinev;

import com.google.gson.Gson;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.Scanner;

public class CreateBlastFieldWindow
{
    @FXML
    private ListView lsvPoints;
    @FXML
    private Button btnCancel;
    @FXML
    private Button btnCreate;
    @FXML
    private TextField txtName;
    @FXML
    private TextField txtBenchElevation;
    @FXML
    private TextField txtBottomBench;
    @FXML
    private TextField txtHolesDiameter;
    @FXML
    private TextField txtSubDrilling;
    @FXML
    private Slider sldPatternA;
    @FXML
    private Label lblPatternA;
    @FXML
    private Slider sldPatternB;
    @FXML
    private Label lblPatternB;
    @FXML
    private Slider sldHolesInclination;
    @FXML
    private Label lblHolesInclination;
    @FXML
    private Button btnClearRow;
    @FXML
    private Button btnClearList;
    @FXML
    private Button btnInsertFile;

    public void initialize()
    {
        txtSubDrilling.setDisable(true);
        txtBottomBench.setDisable(true);

        lsvPoints.getItems().add("1\t4611826.684\t8556125.929\t857.309");
        lsvPoints.getItems().add("2\t4611827.272\t8556125.774\t857.326");
        lsvPoints.getItems().add("3\t4611827.526\t8556126.752\t857.245");
        lsvPoints.getItems().add("4\t4611826.991\t8556126.977\t857.289");
        lsvPoints.getItems().add("5\t4611827.526\t8556126.752\t857.245");
        lsvPoints.getItems().add("6\t4611826.991\t8556126.977\t857.289");
        lsvPoints.getItems().add("7\t4611827.526\t8556126.752\t857.245");
        lsvPoints.getItems().add("8\t4611826.991\t8556126.977\t857.289");
        lsvPoints.getItems().add("9\t4611827.526\t8556126.752\t857.245");
        lsvPoints.getItems().add("10\t4611826.991\t8556126.977\t857.289");
        lsvPoints.getItems().add("11\t4611827.526\t8556126.752\t857.245");
        lsvPoints.getItems().add("12\t4611826.991\t8556126.977\t857.289");
    }

    @FXML
    public void handleKeyReleasetxtName()
    {
        String text = txtName.getText();
        if (text.length() > 100) {
            txtName.setStyle("-fx-text-inner-color: red");
        } else {
            txtName.setStyle("-fx-text-inner-color: black");

        }
    }

    @FXML
    public void handleKeyReleasedtxtBenchElevation()
    {
        String text = txtBenchElevation.getText();
//        https://regex101.com/r/aJ3sP7/1 --> regular expression site
        if (!text.matches("^-?(?:\\d+|\\d{1,3}(?:,\\d{3})+)(?:(\\.|,)\\d+)?$")) {
            txtBenchElevation.setStyle("-fx-text-inner-color: red");
            //txtBenchElevation.clear();
        } else {
            txtBenchElevation.setStyle("-fx-text-inner-color: black");

        }
        boolean disable = text.isEmpty() || text.trim().isEmpty();
        txtBottomBench.setDisable(disable);
    }
//  TODO: Да приема само цифри и десетична запетая. Както е по- горният метод за Bench Elevation
    @FXML
    public void handleKeyReleasedtxtBottomBench()
    {
        Double bottomBench = Double.valueOf(txtBottomBench.getText());
        Double benchElevation = Double.valueOf(txtBenchElevation.getText());
        if (bottomBench <= benchElevation) {
            txtBottomBench.clear();
        }
    }

    @FXML
    public void onSliderHolesInclinationChanged()
    {
        int sliderValue = (int) sldHolesInclination.getValue();
        System.out.println("Holes inclination slider value is: " + sliderValue);
        if (sliderValue < 9) {
            lblHolesInclination.setText("0" + String.valueOf(sliderValue));

        } else {
            lblHolesInclination.setText(String.valueOf(sliderValue));
        }
    }

    @FXML
    public void onSliderPatternAChanged()
    {
        double sliderValue = (double) sldPatternA.getValue();
        String sliderValueStr = String.format("%.1f", sliderValue);
        System.out.println("Pattern A slider value is: " + sliderValueStr);
        lblPatternA.setText(sliderValueStr);
    }

    @FXML
    public void onSliderPatternBChanged()
    {
        double sliderValue = (double) sldPatternB.getValue();
        String sliderValueStr = String.format("%.1f", sliderValue);
        System.out.println("Pattern B slider value is: " + sliderValueStr);
        lblPatternB.setText(sliderValueStr);
    }

    @FXML
    public void click_btnInsertFile() throws InvocationTargetException
    {
        lsvPoints.getItems().clear();
        StringBuilder line = new StringBuilder();
        String pointLine;

        int holeNumber;
        double holeNorth;
        double holeEast;
        double holeElevation;

//        create fileChooser
        FileChooser fileChooser = new FileChooser();

        fileChooser.setInitialDirectory(new File("C:\\Users\\emild\\Desktop\\JavaUdemy\\BlastFxMaven"));
        fileChooser.getExtensionFilters().addAll(new FileChooser.ExtensionFilter("TXT Files", "*.txt"));

        File file = fileChooser.showOpenDialog(btnInsertFile.getScene().getWindow());
        System.out.println(file);
        try (Scanner input = new Scanner(file)){
            while (input.hasNextLine()) {
                line.append(input.nextLine());
                pointLine = String.valueOf(line);

                lsvPoints.getItems().add(pointLine);
                line.setLength(0);
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        String text = String.valueOf(line);
        System.out.println(text);

    }

    @FXML
    public void click_btnClearList()
    {
        lsvPoints.getItems().clear();
    }

    @FXML
    public void click_btnClearRow()
    {
        int row = lsvPoints.getSelectionModel().getSelectedIndex();
        if (row < 0) {
            System.out.println("List is empty");
        } else {
            lsvPoints.getItems().remove(row);
        }
    }


    @FXML
    public void click_btnCreate() throws IOException, ClassNotFoundException
    {
        String fieldName = "VP1.1 zadadeno 26/08/20 vzriveno 28/08/20";
        double benchElevation = 710.0;
        double bottomBench = 700.0;
        int holesDiameter = 115;
        double subDrilling = 0.5;
        int holesInclination = 0;
        double patternA = 3.5;
        double patternB = 3.5;

        DrillField<CoordinatePoint> newDrillField = new DrillField<>(fieldName, benchElevation, bottomBench, holesDiameter,
                subDrilling, holesInclination, patternA, patternB);

        int listSize = lsvPoints.getItems().size();
        if (listSize > 0) {
            for (int i = 0; i < listSize; i++) {
                String drillPoint = (String) lsvPoints.getItems().get(i);
                String[] elements = drillPoint.split("\t");
                int number = Integer.parseInt(elements[0]);
                double north = Double.parseDouble(elements[1]);
                double east = Double.parseDouble(elements[2]);
                double elevation = Double.parseDouble(elements[3]);

                newDrillField.addPoint(new DrillPoint(number, north, east, elevation));
                System.out.println();
            }
        }

        Gson gson = new Gson();
        gson.toJson(newDrillField, new FileWriter("newObject"));




        System.out.println(newDrillField.getPointsCount());




        if (txtName.getStyle() == "-fx-text-inner-color: red") {
            txtName.setText("!!! Invalid input !!! Маx 20 chars");
        }
    }


    public void click_btnCancel()
    {
        Stage stage = (Stage) btnCancel.getScene().getWindow();
        stage.close();
    }


}
